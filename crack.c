#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.

int file_length(char *filename)
{

    struct stat info;
    
    int ret = stat(filename, &info);
    
    if(ret == -1)
        {
            return -1;
        }
    else
        {
            return info.st_size;
        }

}
////////////////////////////////////////////////

int tryguess(char *hash, char *guess)
{
   
    char *guessmd = md5(guess, strlen(guess));
    
    if (strcmp(guessmd,hash) == 0)
        {
            free(guessmd);
            return 1;
        }
    else
        {
            free(guessmd);
            return 0;
        }
//free(guessmd);
     
}
/////////////////////////////////////////////
char **read_dictionary(char *filename, int *size)
{
    int len = file_length(filename);
    char *pass = malloc(len);
     
    FILE *f = fopen(filename,"r");
    if(!f)
        {
            printf(" Can't open %s for reading.\n", filename);
            exit(1);
        }

    fread(pass, sizeof(char), len, f);
    
    fclose(f);

    int numberOfPasswords = 0;

    for(int i = 0; i < len; i++)
        {
            if(pass[i] == '\n')
            {
                pass[i] = '\0';
                numberOfPasswords++;
            }
        }
    // allocate space for array pointers
    char **passwords = malloc(numberOfPasswords * sizeof(char *));

    //load address to array
    passwords[0] = &pass[0];
    int j = 1;
    for(int i = 0; i < len; i++)
        {
            if (pass[i] == '\0')
            {
                passwords[j]= &pass[i+1];
                j++;
            }
        }
     
    *size = numberOfPasswords;
    return passwords;
    
}
//////////////////////////////////////////////////////////////
int main(int argc, char *argv[])
{
    if (argc < 3) 
        {
            printf("Usage: %s hash_file dict_file\n", argv[0]);
            exit(1);
        }
    
    // Read the dictionary file into an array of strings.
    int dlen = 0;
    char **dict = read_dictionary(argv[2], &dlen);

    // Open the hash file for reading.

    FILE *fp = fopen(argv[1], "r");
    if(!fp)
        {
            printf("Cannot open %s file\n",argv[1]);
            exit(1);
        }
   
    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    int count = 0;
    
    char intel[HASH_LEN];
   
    while(fgets(intel, HASH_LEN, fp) != NULL)
        {
           if(intel[strlen(intel)-1]== '\n')
                {
                    intel[strlen(intel)-1] = '\0';
                }
            for (int i = 0; i < dlen; i++)
                {
                    //printf("%s", intel);
                    if (tryguess(intel, dict[i]) == 1)
                    {
                        printf("Match: %s : %s\n", dict[i], intel);
                        count++;
                        break;
                    }
                }
        
        }
    printf("%d passwords found.\n",count);
    fclose(fp);
}